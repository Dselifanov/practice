package Practice.main.controller;

import Practice.main.dto.DocumentDto;
import Practice.main.service.DocumentService;
import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/documents")
@RequiredArgsConstructor
public class DocumentController {

    private final DocumentService documentService;


    @GetMapping("/{id}")
    public ResponseEntity<DocumentDto> getDocument (@PathVariable("id") Long id){
        return ResponseEntity.ok().body(documentService.findById(id));
    }

    @GetMapping("/all")
    public ResponseEntity<List<DocumentDto>> getAllDocuments (){
        return ResponseEntity.ok().body(documentService.findAll());
    }

    @PostMapping("/add")
    public ResponseEntity<String> createNewDocument (@RequestBody DocumentDto documentDto) throws JsonProcessingException {
        if (documentService.save(documentDto)) {
            return ResponseEntity.ok("Документ сохранен, данные подтверждены");
        }
        return ResponseEntity.ok("Документ не сохранен, перепроверьте ваши данные");
    }



}
