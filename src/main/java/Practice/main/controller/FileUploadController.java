
package Practice.main.controller;

import Practice.main.model.File;
import Practice.main.response.FileResponse;
import Practice.main.service.FileStorageService;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;


@Controller
@RequestMapping()
@RequiredArgsConstructor
public class FileUploadController {


    private final FileStorageService fileStorageService;



    @PostMapping("upload")
    public ResponseEntity<String> saveFile(@RequestParam("file") MultipartFile file) {
        fileStorageService.saveFileToDB(file);
        return ResponseEntity.ok().body("Файл сохранен успешно");
    }



    @GetMapping("/download/{id}")
    public ResponseEntity<Resource> downloadFile (@PathVariable String id) {

        File file = fileStorageService.downloadFileFromDB(Long.parseLong(id));
        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(file.getFileType()))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + file.getFileName())
                .body(new ByteArrayResource(file.getFileData()));

    }

}