package Practice.main.exception;

public class StorageException extends RuntimeException {

	//Rename!

	public StorageException(String message) {
		super(message);
	}

	public StorageException(String message, Throwable cause) {
		super(message, cause);
	}
}
