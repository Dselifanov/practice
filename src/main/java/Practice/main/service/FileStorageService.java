package Practice.main.service;

import Practice.main.exception.StorageException;
import Practice.main.model.File;
import Practice.main.repository.FileRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Service
@RequiredArgsConstructor
public class FileStorageService {


    private final FileRepository fileRepository;


    public File saveFileToDB(MultipartFile file) {

        File uploadedFile = new File();

        try {
            uploadedFile.setFileName(file.getOriginalFilename());
            uploadedFile.setFileType(file.getContentType());
            uploadedFile.setFileData(file.getBytes());

            fileRepository.save(uploadedFile);

            } catch (IOException e) {
            throw new StorageException("Could not upload file!");
        }

        return uploadedFile;
    }



    public File downloadFileFromDB(long id) {
        return fileRepository.getReferenceById(id);
        //return fileRepository.getOne(id);
    }


    public File findById(long id) {
        File file = new File();
        if (fileRepository.existsById(id)){
            file = fileRepository.findById(id).get();
            return file;
        } else {
            System.out.println("No such file with id=" +id);
        }
        return null;
    }


}