
package Practice.main.service;

import Practice.main.dto.CredentialDto;
import Practice.main.mapper.CredentialMapper;
import Practice.main.repository.CredentialRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
public class CredentialService {


    private final CredentialRepository repository;
    private final CredentialMapper credentialMapper;

    @Transactional
    public boolean save (CredentialDto credentialDto) {

        repository.save(credentialMapper.toEntity(credentialDto));
        return true;
    }

    @Transactional(readOnly = true)
    public List<CredentialDto> findAll () {
        return repository.findAll()
                .stream()
                .map(credentialMapper::toDto)
                .collect(Collectors.toList());}


}
