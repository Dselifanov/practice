package Practice.main.service;

import Practice.main.dto.TemplateDto;
import Practice.main.mapper.TemplateMapper;
import Practice.main.repository.TemplateRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TemplateService {


    private final TemplateRepository repository;
    private final TemplateMapper templateMapper;

    @Transactional
    public void save (TemplateDto templateDto) {
        repository.save(templateMapper.toEntity(templateDto));}

    @Transactional(readOnly = true)
    public List<TemplateDto> findAll () {
        return repository.findAll()
                .stream()
                .map(templateMapper::toDto)
                .collect(Collectors.toList());}

    @Transactional(readOnly = true)
    public TemplateDto findById (Long id) {
        return templateMapper.toDto(repository.findById(id).orElse(null));}

    @Transactional
    public void deleteById (Long id) {repository.deleteById(id);}

}
