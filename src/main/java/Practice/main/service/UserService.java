package Practice.main.service;

import Practice.main.dto.UserDto;
import Practice.main.mapper.UserMapper;
import Practice.main.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserService {


    private final UserRepository repository;
    private final UserMapper userMapper;


    @Transactional
    public void save (UserDto userDto) {
        repository.save(userMapper.toEntity(userDto));}


    @Transactional(readOnly = true)
    public List<UserDto> findAll () {
        return repository.findAll()
                .stream()
                .map(userMapper::toDto)
                .collect(Collectors.toList());}

    @Transactional(readOnly = true)
    public UserDto findById (Long id) {
        return userMapper.toDto(repository.findById(id).orElse(null));}

    @Transactional
    public void deleteById (Long id) {repository.deleteById(id);}

}
