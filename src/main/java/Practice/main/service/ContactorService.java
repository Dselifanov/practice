
package Practice.main.service;

import Practice.main.dto.ContactorDto;
import Practice.main.mapper.ContactorMapper;
import Practice.main.repository.ContactorRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ContactorService {


    private final ContactorRepository repository;
    private final ContactorMapper contactorMapper;

    @Transactional
    public boolean save (ContactorDto contactorDto) {

        repository.save(contactorMapper.toEntity(contactorDto));
        return true;
    }

    @Transactional(readOnly = true)
    public List<ContactorDto> findAll () {
        return repository.findAll()
                .stream()
                .map(contactorMapper::toDto)
                .collect(Collectors.toList());}


}

