package Practice.main.service;

import Practice.main.dto.DocumentDto;
import Practice.main.mapper.DocumentMapper;
import Practice.main.repository.DocumentRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class DocumentService {


    private final DocumentRepository repository;
    private final DocumentMapper documentMapper;
    private final GetINNService getINNService;


    @Transactional
    public boolean save (DocumentDto documentDto) throws JsonProcessingException {
        if (getINNService.getINN(documentDto.getContactor())){

            repository.save(documentMapper.toEntity(documentDto));
            return true;
        }
        else{
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    "Введенные данные не верны, пожалуйста, проверьте введенные данные и повторите попытку.");
        }
    }


    @Transactional(readOnly = true)
    public DocumentDto findById (Long id) {
        return documentMapper.toDto(repository.findById(id).orElse(null));
    }


    @Transactional(readOnly = true)
    public List<DocumentDto> findAll () {
        return repository.findAll()
                .stream()
                .map(documentMapper::toDto)
                .collect(Collectors.toList());}


}
