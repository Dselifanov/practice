package Practice.main.service;

import Practice.main.dto.FieldDto;
import Practice.main.mapper.FieldMapper;
import Practice.main.repository.FieldRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class FieldService {


    private final FieldRepository repository;
    private final FieldMapper fieldMapper;


    @Transactional
    public void save (FieldDto fieldDto) {
        repository.save(fieldMapper.toEntity(fieldDto));}

    @Transactional(readOnly = true)
    public List<FieldDto> findAll () {
        return repository.findAll()
                .stream()
                .map(fieldMapper::toDto)
                .collect(Collectors.toList());}

    @Transactional(readOnly = true)
    public FieldDto findById (Long id) {
        return fieldMapper.toDto(repository.findById(id).orElse(null));}

    @Transactional
    public void deleteById (Long id) {
        repository.deleteById(id);}

}
