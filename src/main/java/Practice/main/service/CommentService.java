package Practice.main.service;

import Practice.main.dto.CommentDto;
import Practice.main.mapper.CommentMapper;
import Practice.main.repository.CommentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CommentService {


    private final CommentRepository repository;
    private final CommentMapper commentMapper;


    @Transactional
    public void save (CommentDto commentDto) {
        repository.save(commentMapper.toEntity(commentDto));}

    @Transactional(readOnly = true)
    public List<CommentDto> findAll () {
        return repository.findAll()
                .stream()
                .map(commentMapper::toDto)
                .collect(Collectors.toList());}

    @Transactional(readOnly = true)
    public CommentDto findById (Long id) {
        return commentMapper.toDto(repository.findById(id).orElse(null));}

    @Transactional
    public void deleteById (Long id) {
        repository.deleteById(id);}

}
