
package Practice.main.repository;

import Practice.main.model.Contactor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContactorRepository extends JpaRepository<Contactor, Long> {

}

