package Practice.main.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DocumentDto {

    private Long id;

    private String documentName;

    private String documentNumber;

    private ContactorDto contactor;

    private List<TemplateDto> template;


}
