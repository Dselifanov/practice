package Practice.main.dto;

import lombok.Data;

import java.util.List;

@Data
public class TemplateDto {


    private Long id;

    private String title;

    private String version;

    private List<FieldDto> field;



}
