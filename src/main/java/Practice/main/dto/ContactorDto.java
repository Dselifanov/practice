
package Practice.main.dto;

import lombok.Data;

import java.util.List;

@Data
public class ContactorDto {

    private Long id;

    private String firstName;

    private String lastName;

    private String patronymic;

    private int phoneNumber;

    private String telegram;

    private String email;

    private String country;

    private String birthday;

    private CredentialDto credential;

    private List<CommentDto> comment;


}

