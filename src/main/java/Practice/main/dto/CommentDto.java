package Practice.main.dto;

import lombok.Data;

@Data
public class CommentDto {

    private Long id;

    private String text;

}
