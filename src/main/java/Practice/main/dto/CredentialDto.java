
package Practice.main.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CredentialDto {

    private Long id;

    private String text;

    private String version;

    private String passport;

    private String inn;

    private String type;


}

