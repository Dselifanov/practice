package Practice.main.dto;

import lombok.Data;

@Data
public class UserDto {

    private Long id;

    private String login;

    private String role;

    private String firstName;

    private String lastName;

    private String patronymic;

    private String password;

    private String telegram;

    private String email;


}
