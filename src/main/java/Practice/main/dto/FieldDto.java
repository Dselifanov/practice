package Practice.main.dto;

import lombok.Data;

@Data
public class FieldDto {


    private Long id;

    private String title;

    private String type;

    private String placeholder;

    private String defaultValue;




}
