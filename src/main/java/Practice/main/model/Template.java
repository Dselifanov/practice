package Practice.main.model;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@SuperBuilder
@Entity
public class Template extends ParentClass {


    private String title;

    private String version;

    @ManyToOne(cascade = CascadeType.PERSIST)
    private Document document;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Field> field;


}
