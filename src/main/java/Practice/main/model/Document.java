package Practice.main.model;


import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@SuperBuilder
@Entity
public class Document extends ParentClass{


    private String documentName;

    private String documentNumber;

    @ManyToOne(cascade = CascadeType.PERSIST)
    private Contactor contactor;


    @OneToMany(cascade = CascadeType.PERSIST)
    private List<Template> template;


}
