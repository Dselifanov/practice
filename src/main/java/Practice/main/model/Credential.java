
package Practice.main.model;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.OneToOne;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@SuperBuilder
@Entity
public class Credential extends ParentClass{


    private String text;

    private String version;

    private String passport;

    private String inn;

    private String type;

    @OneToOne(cascade = CascadeType.ALL)
    private Contactor contactor;




}

