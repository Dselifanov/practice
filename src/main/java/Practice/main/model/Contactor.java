
package Practice.main.model;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@SuperBuilder
@Entity
public class Contactor extends ParentClass {

    private String lastName;

    private String firstName;

    private String patronymic;

    private int phoneNumber;

    private String telegram;

    private String email;

    private String country;

    private String birthday;

    @OneToMany(cascade = CascadeType.PERSIST)
    private List<Document> document;

    @OneToOne(cascade = CascadeType.ALL)
    private Credential credential;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Comment> comment;


}

