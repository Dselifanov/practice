package Practice.main.model;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.Date;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@SuperBuilder
@Entity
public class User extends ParentClass {

    private String login;

    private String role;

    private String firstName;

    private String lastName;

    private String patronymic;

    private String telegram;

    private String email;

    private Date lastVisit;

    private String password;


}
