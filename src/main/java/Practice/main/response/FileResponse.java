package Practice.main.response;

import lombok.Data;

@Data
public class FileResponse {

    private String message;
    private long fileId;
    private String fileName;
    private String fileType;
    private String fileUri;


}
