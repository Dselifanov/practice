package Practice.main.mapper;

import Practice.main.dto.TemplateDto;
import Practice.main.model.Template;
import org.mapstruct.Mapper;


@Mapper
public interface TemplateMapper {

    TemplateDto toDto (Template entity);
    Template toEntity (TemplateDto dto);

}
