
package Practice.main.mapper;

import Practice.main.dto.ContactorDto;
import Practice.main.model.Contactor;
import org.mapstruct.Mapper;


@Mapper
public interface ContactorMapper {

    ContactorDto toDto (Contactor entity);
    Contactor toEntity (ContactorDto dto);
}

