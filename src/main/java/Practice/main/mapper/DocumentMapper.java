package Practice.main.mapper;

import Practice.main.dto.DocumentDto;
import Practice.main.model.Document;
import org.mapstruct.Mapper;


@Mapper
public interface DocumentMapper {

    DocumentDto toDto (Document entity);

    Document toEntity (DocumentDto dto);

}
