
package Practice.main.mapper;

import Practice.main.dto.CredentialDto;
import Practice.main.model.Credential;
import org.mapstruct.Mapper;


@Mapper
public interface CredentialMapper {

    CredentialDto toDto (Credential entity);
    Credential toEntity (CredentialDto dto);
}

