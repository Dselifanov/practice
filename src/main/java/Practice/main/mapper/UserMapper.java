package Practice.main.mapper;

import Practice.main.dto.UserDto;
import Practice.main.model.User;
import org.mapstruct.Mapper;


@Mapper
public interface UserMapper {

    UserDto toDto (User entity);

    User toEntity (UserDto dto);
}
