package Practice.main.mapper;

import Practice.main.dto.CommentDto;
import Practice.main.model.Comment;
import org.mapstruct.Mapper;


@Mapper
public interface CommentMapper {
    CommentDto toDto (Comment entity);
    Comment toEntity (CommentDto dto);
}
