package Practice.main.mapper;

import Practice.main.dto.FieldDto;
import Practice.main.model.Field;
import org.mapstruct.Mapper;

@Mapper
public interface FieldMapper {

    FieldDto toDto (Field entity);

    Field toEntity (FieldDto dto);
}
